
export default defineNuxtConfig({
  css: [
    '@fortawesome/fontawesome-free/css/all.css',
    '@vuepic/vue-datepicker/dist/main.css',
    'vuetify/lib/styles/main.sass',
    "@/assets/styles/main.scss",
  ],
  build: {
    transpile: ['vuetify'],
  },
  vite: {
    define: {
      'process.env.DEBUG': false,
    },
  },
})