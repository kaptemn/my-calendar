# My Calendar

## Demo 

Here is a live demo page and the repo for it:
[https://mycalendar-app-rdes6.ondigitalocean.app/calendar](https://mycalendar-app-rdes6.ondigitalocean.app/calendar)

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```
