import { format } from "date-fns";
import { utcToZonedTime } from "date-fns-tz";

export function getFormatDateTimezone(date: Date, timeZone?: string) {
  if (!timeZone) {
    return format(date, 'HH:mm:ss dd.MM.uuuu');
  }
  
  return format(utcToZonedTime(date, timeZone), 'HH:mm:ss dd.MM.uuuu');
}
